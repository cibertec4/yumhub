/*Funcion para hacer funcionar el slider*/

const carouselImg = [
  "img1.webp",
  "img2.jpg",
  "img3.jpg",
  "img4.webp",
  "img5.jpg",
];

var count = 0;
function mostrarSlider() {
  if (count >= carouselImg.length) count = 0;
  const imageElement = document.getElementById("carouselImg");
  imageElement.src = "../../assets/banner-imagenes/" + carouselImg[count];

  imageElement.classList.remove("carouselImg");

  count += 1;

  setTimeout("mostrarSlider()", 3000);
}

mostrarSlider();

document.body.setAttribute("onload", "mostrarSlider()");

/* --------------------- Funcion para mostrar imagenes en el home --------------------- */

const imagenes = [
  {
    img: "ingredientes.jpg",
    descripcion: "Los  mejores platillos, con los mejores ingredientes.",
  },
  {
    img: "higiene.jpg",
    descripcion: "Los mejores protocolos de seguridad e higiene.",
  },
  {
    img: "innovacion.jpeg",
    descripcion: "Platillos innovadores, con un increible sabor y textura.",
  },
  {
    img: "eco-amigable.jpg",
    descripcion:
      "Utilizamos envases eco-amigables, para garantizar un planeta más limpio.",
  },
];

function generarDivs() {
  const container = document.getElementById("container-images");

  imagenes.forEach((imagen, index) => {
    const div = document.createElement("div");
    div.classList.add("img-container");

    // Agregar clase "impar" o "par" según el índice
    if (index % 2 === 0) {
      div.classList.add("par");
    } else {
      div.classList.add("impar");
    }

    const img = document.createElement("img");
    img.classList.add("img-grande");
    img.src = "../../assets/home-imagenes/" + imagen.img;

    const p = document.createElement("p");
    p.classList.add("text-img");
    p.textContent = imagen.descripcion;

    div.appendChild(img);
    div.appendChild(p);
    container.appendChild(div);
  });
}

generarDivs();
