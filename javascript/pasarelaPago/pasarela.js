function validateNumberInput(event) {
    // Bloquear la entrada de caracteres no numéricos
    if (!/^[0-9]+$/.test(event.data)) {
      event.target.value = event.target.value.replace(/\D/g, '');
    }
  }
  
  function validateForm() {
    var cardNumber = document.getElementById("card-number").value;
    var cardHolder = document.getElementById("card-holder").value;
    var expirationDate = document.getElementById("expiration-date").value;
    var cvv = document.getElementById("cvv").value;
  
    // Validar número de tarjeta
    var cardNumberRegex = /^[1-9][0-9]{15}$/;
    if (!cardNumberRegex.test(cardNumber)) {
      alert("El número de tarjeta es inválido.");
      return false;
    }
  
    // Validar nombre del titular
    if (cardHolder.trim() === "") {
      alert("Ingrese el nombre del titular de la tarjeta.");
      return false;
    }
  
    // Validar fecha de caducidad
    var expirationDateRegex = /^(0[1-9]|1[0-2])\/[0-9]{2}$/;
    if (!expirationDateRegex.test(expirationDate)) {
      alert("La fecha de caducidad es inválida.");
      return false;
    }
  
    // Validar CVV
    var cvvRegex = /^[0-9]{3}$/;
    if (!cvvRegex.test(cvv)) {
      alert("El CVV es inválido.");
      return false;
    }
  
    // Cálculo del subtotal y mostrar mensaje de éxito
    var price = 100; // Precio del producto (ejemplo)
    var subtotal = price; // Calcular el subtotal según tu lógica
    document.getElementById("subtotal").textContent = subtotal;
    alert("¡Compra realizada con éxito! Subtotal: $" + subtotal.toFixed(2));
    return true;
  }
  